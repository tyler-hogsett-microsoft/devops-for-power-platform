param(
    [Parameter(Mandatory=$true)]
    [string]$userName,
    [Parameter(Mandatory=$true)]
    [string]$repositoryName
)

$zipFilePath = "$repositoryName.zip"
$contentsPath = "$repositoryName-contents"

function Remove-Artifacts {
    Remove-Item `
        $zipFilePath `
        -ErrorAction Ignore
    Remove-Item `
        $contentsPath `
        -Force `
        -Recurse `
        -ErrorAction Ignore
}

Remove-Artifacts

$url = "https://api.github.com/repos/$userName/$repositoryName/zipball/main"
$response = Invoke-WebRequest -Uri $url
if($response.StatusCode -ne 200) {
    $proxyUri = [Uri]$null
    $proxy = [System.Net.WebRequest]::GetSystemWebProxy()
    $proxy.Credentials = [System.Net.CredentialCache]::DefaultCredentials
    $proxyUri = $proxy.GetProxy($url)
    $response = Invoke-WebRequest `
        -Uri $url `
        -Proxy $proxyUri `
        -ProxyUseDefaultCredentials
}
[System.IO.File]::WriteAllBytes(
    (Join-Path $pwd $zipFilePath),
    $response.Content
)

Expand-Archive `
    $zipFilePath `
    $contentsPath
Remove-Item $repositoryName `
    -Force `
    -Recurse `
    -ErrorAction Ignore
New-Item $repositoryName `
    -ItemType directory
Move-Item `
    "$contentsPath/$userName-$repositoryName-*/*" `
    $repositoryName

Remove-Artifacts
