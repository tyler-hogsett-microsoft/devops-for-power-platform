import fetch from "node-fetch";
import { chmod, stat } from "fs/promises";
import { dirname, join } from "path";
import unzip from "./unzip.js";
import { fileURLToPath } from "url";

const id = "Microsoft.PowerApps.CLI.Core.linux-x64".toLowerCase();

fetchPowerPlatformCli();

async function fetchPowerPlatformCli() {
  const path = join(getDirName(), "..", "..", "pac");
  if (await pathExists(join(path, "pac"))) {
    return;
  }

  const version = await getLatestVersion();
  const buffer = await getBuffer(version);
  await unzip(buffer, path, {
    include: /^tools\//,
    pathTransformer: (path) => path.replace(/^tools\//, ""),
  });
  await chmod(join(path, "pac"), 0x777);

  return path;
}

async function pathExists(path: string) {
  try {
    await stat(path);
    return true;
  } catch (error: any) {
    if (error.code === "ENOENT") {
      return false;
    } else {
      throw error;
    }
  }
}

async function getLatestVersion() {
  const response = await fetch(
    `https://api.nuget.org/v3/registration5-semver1/${id}/index.json`
  );
  const contents = (await response.json()) as any;
  return contents.items[0].upper;
}

async function getBuffer(version: string) {
  const response = await fetch(
    `https://api.nuget.org/v3-flatcontainer/${id}/${version}/${id}.${version}.nupkg`
  );
  const buffer = await response.buffer();
  return buffer;
}

function getDirName() {
  const fileName = fileURLToPath(import.meta.url);
  const dirName = dirname(fileName);
  return dirName;
}
