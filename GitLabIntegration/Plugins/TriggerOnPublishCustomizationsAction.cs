﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Net.Http;

namespace GitLabIntegration.Plugins
{
    public class TriggerOnPublishCustomizationsAction : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            var serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            var service = serviceFactory.CreateOrganizationService(null);

            var triggerUrl = GetCommitFlowTriggerUrl(service);

            TriggerPipeline(triggerUrl);
        }

        private string GetCommitFlowTriggerUrl(IOrganizationService service)
        {
            var query = new QueryExpression("environmentvariablevalue")
            {
                ColumnSet = new ColumnSet
                (
                    "value"
                ),
                Criteria = new FilterExpression(),
                TopCount = 1
            };
            query.Criteria.AddCondition(
                "statecode",
                ConditionOperator.Equal,
                0
            );
            var variableLink = query.AddLink(
                "environmentvariabledefinition",
                "environmentvariabledefinitionid",
                "environmentvariabledefinitionid"
            );
            variableLink.LinkCriteria.AddCondition(
                "schemaname",
                ConditionOperator.Equal,
                "msce_CommitFlowTriggerURL"
            );
            var results = service.RetrieveMultiple(query);
            if(results.Entities.Count == 0)
            {
                throw new InvalidPluginExecutionException(
                    "There is no current values for the \"Commit Flow Trigger URL\" environment variable."
                );
            }
            var record = results.Entities[0];
            return record.GetAttributeValue<string>("value");
        }

        private void TriggerPipeline(string triggerUrl)
        {
            var client = new HttpClient();
            var uri = new Uri(triggerUrl);
            client.PostAsync(
                uri,
                new StringContent("")
            );
        }
    }
}
