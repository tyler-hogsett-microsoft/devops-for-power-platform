﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLabIntegration.Plugins
{
    class GitLabConfiguration
    {
        public GitLabConfiguration(string projectId, string triggerToken)
        {
            ProjectId = projectId;
            TriggerToken = triggerToken;
        }

        public string ProjectId { get; }
        public string TriggerToken { get; }
    }
}
